jest.mock('../../../../lib/validation/validators', () => ({
  isValid: jest.fn().mockReturnValue(true),
}));
jest.mock('../../../../lib/validation/scholarlyArticle', () => ({
  scholarlyArticleNameValidators: [],
}));
jest.mock('../../src/services/scholarlyArticle', () => ({
  fetchScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve({
    creatorSessionId: 'Arbitrary session ID',
  })),
}));
jest.mock('../../src/services/updateScholarlyArticle', () => ({
  updateScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve({
    name: 'Arbitrary name',
  })),
}));
jest.mock('../../../../lib/utils/periodicals', () => ({
  isArticleAuthor: jest.fn().mockReturnValue(true),
}));

import { updateScholarlyArticle } from '../../src/resources/updateScholarlyArticle';

const mockContext = {
  body: { object: { name: 'Arbitrary name' }, targetCollection: { identifier: 'Arbitrary article ID' } },
  database: {} as any,
  session: { identifier: 'Arbitrary session ID' },

  headers: {},
  method: 'PUT' as 'PUT',
  params: [ '/articles/00000000-0000-0000-0000-000000000000', '00000000-0000-0000-0000-000000000000' ],
  path: '/articles/00000000-0000-0000-0000-000000000000',
  query: null,
};

it('should error when no properties to update were specified', () => {
  const promise = updateScholarlyArticle({ ...mockContext, body: {} });

  return expect(promise).rejects.toEqual(new Error('Please specify properties to update.'));
});

it('should error when the user does not have a session', () => {
  const promise = updateScholarlyArticle({
    ...mockContext,
    session: new Error('No session found'),
  });

  return expect(promise).rejects.toEqual(new Error('No session found'));
});

it('should error when no Journal ID was specified', () => {
  const promise = updateScholarlyArticle({
    ...mockContext,
    body: {
      object: mockContext.body.object,
      targetCollection: {} as any,
    },
  });

  return expect(promise).rejects.toEqual(new Error('No article to update specified'));
});

it('should error when the given article could not be retrieved', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.reject(new Error('Some error')));

  const promise = updateScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('Some error'));
});

it('should error when the given article could not be found', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve(null));

  const promise = updateScholarlyArticle({
    ...mockContext,
    body: {
      ...mockContext.body,
      targetCollection: {
        ...mockContext.body.targetCollection,
        identifier: 'some_id',
      },
    },
  });

  return expect(promise).rejects.toEqual(new Error('Could not find an article with ID some_id.'));
});

it('should error when the given article was not initialised by the current user', () => {
  const promise = updateScholarlyArticle({
    ...mockContext,
    session: { identifier: 'Arbitrary non-owner identifier' },
  });

  return expect(promise).rejects.toEqual(new Error('You can only update articles you are an author of.'));
});

it('should error when the given article is not authored by the current user', () => {
  const mockedIsArticleAuthor = require.requireMock('../../../../lib/utils/periodicals').isArticleAuthor;
  mockedIsArticleAuthor.mockReturnValueOnce(false);
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    author: [ {
      identifier: 'Arbitrary author ID',
    } ],
  }));

  const promise = updateScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only update articles you are an author of.'));
});

it('should not store invalid properties', (done) => {
  const mockedService = require.requireMock('../../src/services/updateScholarlyArticle');
  const mockedValidator = require.requireMock('../../../../lib/validation/validators');

  mockedValidator.isValid.mockReturnValueOnce(false);
  mockedValidator.isValid.mockReturnValueOnce(false);

  const promise = updateScholarlyArticle(mockContext);

  setImmediate(() => {
    // The third argument is the properties to update.
    // Since they're all invalid, this should be empty.
    expect(mockedService.updateScholarlyArticle.mock.calls[0][2]).toEqual({});

    done();
  });
});

it('should error when the service errors', () => {
  const mockedService = require.requireMock('../../src/services/updateScholarlyArticle');
  mockedService.updateScholarlyArticle.mockReturnValueOnce(Promise.reject(new Error('Some error')));

  const promise = updateScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('There was a problem modifying the article, please try again.'));
});
