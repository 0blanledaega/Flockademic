import { Account } from '../../../../lib/interfaces/Account';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function makePublic(
  database: Database,
  targetIdentifier: string,
  proposedIdentifier: string,
  account: Account,
): Promise<{ identifier: string; creator: Account; }> {
  await database.tx((t) => {
    const queries = [
      // Make sure the current account is owner of the periodical
      t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'INSERT INTO periodical_creators (periodical, creator) VALUES (${periodicalId}, ${creatorId})'
          + ' ON CONFLICT (periodical, creator) DO NOTHING',
        { periodicalId: targetIdentifier, creatorId: account.identifier },
      ),
      // Publish the periodical
      t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'UPDATE periodicals SET date_published=NOW(), identifier=${proposedIdentifier}'
          // tslint:disable-next-line:no-invalid-template-strings
          + ' WHERE identifier=${targetIdentifier}',
        { proposedIdentifier, targetIdentifier },
      ),
    ];

    return t.batch(queries);
  });

  return {
    creator: { identifier: account.identifier },
    identifier: proposedIdentifier,
  };
}
