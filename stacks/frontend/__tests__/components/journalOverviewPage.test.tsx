jest.mock('../../src/services/periodical', () => ({
  getPeriodical: jest.fn().mockReturnValue(Promise.resolve(
    {
      creator: { identifier: 'arbitrary_account_id' },
      identifier: 'arbitrary_slug',
      name: 'Arbitrary name',
    },
  )),
  getPeriodicalContents: jest.fn().mockReturnValue(Promise.resolve({
    hasPart: [],
  })),
}));
jest.mock('../../src/services/account', () => ({
  getProfiles: jest.fn().mockReturnValue(Promise.resolve([
    { identifier: 'arbitrary_account_id', name: 'Arbitrary name', sameAs: 'https://orcid.org/arbitrary_orcid' },
  ])),
  getSession: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary user id' })),
}));

import {
  JournalOverviewPage,
  JournalOverviewPageRouteParams,
} from '../../src/components/journalOverviewPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

function initialisePage({
  slug = 'arbitrary_slug',
}: Partial<JournalOverviewPageRouteParams> = {}) {
  return mount(
    <JournalOverviewPage match={{ params: { slug }, url: 'https://arbitrary_url' }}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the Journal details are loading', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display an error message when the journal could not be loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('.alert')).toExist();
    done();
  }
});

it('should pass null to the journal overview when no articles could be loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodicalContents.mockReturnValueOnce(Promise.reject('Service error'));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('JournalOverview').prop('articles')).toBeNull();
    done();
  }
});

it('should not pass a session to the journal overview when no session details could be loaded', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getSession.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('JournalOverview').prop('session')).toBeUndefined();
    done();
  }
});

it('should not pass a list of editor details to the journal overview when they could not be loaded', (done) => {
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(() => {
    // Journal details fetched, now fetch the profiles
    page.update();
    setImmediate(checkAssertions);
  });
  function checkAssertions() {
    page.update();
    expect(page.find('JournalOverview').prop('periodical').creator.name).toBeUndefined();
    done();
  }
});

it('should not fetch account details when no journal author was defined', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.resolve({
    creator: undefined,
  }));
  const mockAccountService = require.requireMock('../../src/services/account');

  const page = initialisePage();

  setImmediate(() => {
    // Journal details fetched, now fetch the profiles
    page.update();
    setImmediate(checkAssertions);
  });
  function checkAssertions() {
    page.update();
    expect(mockAccountService.getProfiles.mock.calls.length).toBe(0);
    done();
  }
});

it('should pass a list of editor details to the journal overview', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.resolve({
    creator: { identifier: 'arbitrary_account_id' },
  }));
  const mockAccountService = require.requireMock('../../src/services/account');
  mockAccountService.getProfiles.mockReturnValueOnce(Promise.resolve([
    { identifier: 'arbitrary_account_id', name: 'Some name', sameAs: 'https://orcid.org/arbitrary_orcid' },
  ]));

  const page = initialisePage();

  setImmediate(() => {
    // Journal details fetched, now fetch the profiles
    page.update();
    setImmediate(checkAssertions);
  });
  function checkAssertions() {
    page.update();
    expect(page.find('JournalOverview').prop('periodical').creator.name).toBe('Some name');
    done();
  }
});
